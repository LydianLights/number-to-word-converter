using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NumbersToWords.Models;

namespace NumbersToWords.Models.Tests
{
  [TestClass]
  public class NumberToWordConverterTest
  {
    [TestMethod]
    [ExpectedException(typeof(Exception))]
    public void Convert_NumberIsNegative_Exception()
    {
      NumberToWordConverter.Convert(-1);
    }
    [TestMethod]
    public void Convert_NumberIs0_NumberString()
    {
      Assert.AreEqual("zero", NumberToWordConverter.Convert(0));
    }
    [TestMethod]
    public void Convert_NumberIsSingleDigit_NumberString()
    {
      Assert.AreEqual("three", NumberToWordConverter.Convert(3));
    }

    [TestMethod]
    public void Convert_NumberIsBetween10And19_NumberString()
    {
      Assert.AreEqual("thirteen", NumberToWordConverter.Convert(13));
    }
    [TestMethod]
    public void Convert_NumberIsBetween20And99_NumberString()
    {
      Assert.AreEqual("thirty one", NumberToWordConverter.Convert(31));
    }
    [TestMethod]
    public void Convert_NumberIsBetween20And99AndMultOf10_NumberString()
    {
      Assert.AreEqual("sixty", NumberToWordConverter.Convert(60));
    }

    [TestMethod]
    public void Convert_NumberIsBetween100And999_NumberString()
    {
      Assert.AreEqual("six hundred sixty six", NumberToWordConverter.Convert(666));
    }
    [TestMethod]
    public void Convert_NumberIsBetween100And999AndMultOf100_NumberString()
    {
      Assert.AreEqual("four hundred", NumberToWordConverter.Convert(400));
    }
    [TestMethod]
    public void Convert_NumberIsBetween100And999WithSkippedTens_NumberString()
    {
      Assert.AreEqual("four hundred four", NumberToWordConverter.Convert(404));
    }

    [TestMethod]
    public void Convert_NumberIsBetween1000And999999_NumberString()
    {
      Assert.AreEqual("one hundred twenty three thousand four hundred fifty six", NumberToWordConverter.Convert(123456));
    }
    [TestMethod]
    public void Convert_NumberIsBetween1000And999999AndMultipleOf1000_NumberString()
    {
      Assert.AreEqual("three hundred fifty six thousand", NumberToWordConverter.Convert(356000));
    }
    // TODO: Test for millions
  }
}
