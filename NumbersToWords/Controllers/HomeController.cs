using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using NumbersToWords.Models;

namespace NumbersToWords.Controllers
{
    public class HomeController : Controller
    {
      [HttpGet("/")]
      public ActionResult Index()
      {
        return View();
      }
    }
}
